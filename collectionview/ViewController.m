//
//  ViewController.m
//  collectionview
//
//  Created by Prince on 01/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableArray *data;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView1;

@end

@implementation ViewController
@synthesize collectionView1;
- (void)viewDidLoad {
    
    data = [NSMutableArray new];
    for (int i = 0; i<=30000; i++) {
        NSString *strdata = [NSString stringWithFormat:@"%d",i];
        [data addObject:strdata];
    }
    [collectionView1 reloadData];
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return data.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"identifier" forIndexPath:indexPath];
    
    UILabel *label1=(UILabel *)[cell viewWithTag:123];
    
    label1.text= data [indexPath.row];
    
    
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
